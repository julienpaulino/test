# Stakinsat Project

Build a CRUD App example.

## Docker compose
Run `docker-compose up -d` to start a postgresql database.

## Angular app
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Spring Boot app
Run `./gradlew server:bootRun` for a dev server. Backend API are accessible to `http://localhost:8080`.

## Open api
List of the APIs http://localhost:8080/v3/api-docs/
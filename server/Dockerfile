FROM golang:1.17 as golang-build
ENV GO111MODULE=auto
WORKDIR /go/src/application
COPY cmd cmd
RUN go install -v ./...

FROM openjdk:11-jre-slim as java-build
WORKDIR application
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} application.jar
RUN java -Djarmode=layertools -jar application.jar extract
RUN adduser --system --home /var/cache/bootapp --shell /sbin/nologin bootapp;

FROM gcr.io/distroless/java:11
COPY --from=golang-build /go/bin/healthcheck /application/healthcheck
HEALTHCHECK --start-period=10s CMD ["/application/healthcheck"]

COPY --from=java-build /etc/passwd /etc/shadow /etc/
WORKDIR application
COPY --from=java-build application/dependencies/ ./
COPY --from=java-build application/snapshot-dependencies/ ./
COPY --from=java-build application/spring-boot-loader/ ./
COPY --from=java-build application/application/ ./
USER bootapp
ENV _JAVA_OPTIONS "-XX:MaxRAMPercentage=90 -XX:TieredStopAtLevel=1 -Djava.security.egd=file:/dev/./urandom -Djava.awt.headless=true -Dfile.encoding=UTF-8"
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]

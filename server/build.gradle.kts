import org.springframework.boot.gradle.plugin.SpringBootPlugin

plugins {
    id("java")
    id("jacoco")
    id("org.springframework.boot") version "2.6.3"
    id("org.liquibase.gradle") version "2.1.1"
}

dependencies {
    implementation(platform(SpringBootPlugin.BOM_COORDINATES))
    developmentOnly(platform(SpringBootPlugin.BOM_COORDINATES))
    liquibaseRuntime(platform(SpringBootPlugin.BOM_COORDINATES))
    implementation(platform("org.testcontainers:testcontainers-bom:1.16.3"))

    liquibaseRuntime("org.liquibase:liquibase-core")
    liquibaseRuntime("org.liquibase:liquibase-groovy-dsl:3.0.2")
    liquibaseRuntime("info.picocli:picocli:4.6.2")
    liquibaseRuntime("org.yaml:snakeyaml")
    liquibaseRuntime("org.postgresql:postgresql")

    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.liquibase:liquibase-core")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.13.1")
    implementation("org.springdoc:springdoc-openapi-ui:1.6.4")

    runtimeOnly("org.postgresql:postgresql")

    developmentOnly("org.springframework.boot:spring-boot-devtools")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude("org.junit.vintage:junit-vintage-engine" )
    }
    testImplementation("org.testcontainers:junit-jupiter")
    testImplementation("org.testcontainers:postgresql")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
}

// Disable plain archive to be built
// https://docs.spring.io/spring-boot/docs/current/gradle-plugin/reference/htmlsingle/#packaging-executable.and-plain-archives
tasks.jar {
    enabled = false
}

tasks.test {
    useJUnitPlatform()
}

jacoco {
    toolVersion = "0.8.7"
}

// Do not generate reports for individual projects
tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
    }
}

liquibase {
    activities.register("main") {
        this.arguments = mapOf(
            "logLevel" to "info",
            "changeLogFile" to "src/main/resources/db/changelog/db.changelog-master.yaml",
            "url" to "jdbc:postgresql://10.152.183.180:5432/stackinsat",
            "username" to "postgresql",
            "password" to "postgresqlpsw",
            "driver" to "org.postgresql.Driver"
        )
    }
    runList = "main"
}
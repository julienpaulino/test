package com.stackinsat.service;

import com.stackinsat.exception.InvalidAgeException;
import com.stackinsat.model.User;
import com.stackinsat.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User save(User user) {
        if(user.isLessThanOneHundredFiftyYearsOld()) {
            return userRepository.save(user);
        }
        throw new InvalidAgeException("The user have more than 150 years!!");
    }

    public List<User> findAllByAlphabeticalOrder() {
        return new ArrayList<>(userRepository.findAllByOrderByLastNameAsc());
    }
}

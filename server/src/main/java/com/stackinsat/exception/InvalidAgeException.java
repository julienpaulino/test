package com.stackinsat.exception;

public class InvalidAgeException extends RuntimeException {

    public InvalidAgeException(String errorMessage) {
        super(errorMessage);
    }
}

package com.stackinsat.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    void shouldReturnTrueIfUnder150YearsOld() {
        User user = new User("firstname", "lastname", LocalDate.of(1983,10,16));
        assertTrue(user.isLessThanOneHundredFiftyYearsOld());
    }

    @Test
    void shouldReturnFalseIfMoreThan150YearsOld() {
        User user = new User("firstname", "lastname", LocalDate.of(1883,10,16));
        assertTrue(user.isLessThanOneHundredFiftyYearsOld());
    }
}